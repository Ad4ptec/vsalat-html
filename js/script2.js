function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

(function ActiveIcon($) {
	$('.modal').on('hide.bs.modal', function (e) {
		console.log('modal is hidden!');
		$('.panel-collapse.in').collapse('hide');
		$('.recipe__expand').each(function(index, el) {
			$(el).text('Раскрыть рецепт');
		});
	});
	$('.modal').on('show.bs.modal', function (e) {
		var button = e.relatedTarget;
		/*$(button).addClass('no-events');
		setTimeout(function(){
			$(button).removeClass('no-events');
		}, 1700)*/
	});



	$('.sticky-bot-menu > div').click(function(event) {
		$(this).siblings().removeClass('active');
		$(this).addClass('no-events');
		var self = $(this);
		setTimeout(function(){
			self.removeClass('no-events');
		}, 1700)
		$(this).siblings().removeClass('no-events');
		$(this).toggleClass('active');
	});


	$('#modal-lk').on('hidden.bs.modal', function () {
		$('.sticky-bot-menu_user').removeClass('active');
	})
	$('#modal-cart').on('hidden.bs.modal', function() {
		$('.sticky-bot-menu_store').removeClass('active');
	});
	$('#modal-about').on('hidden.bs.modal', function() {
		$('.sticky-bot-menu_info').removeClass('active');
	});
	$('#modal-hamburger').on('hidden.bs.modal', function() {
		$('.sticky-bot-menu_list').removeClass('active');
	});
	$('#modal-search').on('hidden.bs.modal', function() {
		$('.sticky-bot-menu_search').removeClass('active');
	});
	$('#modal-contact').on('hidden.bs.modal', function() {
		$('.sticky-bot-menu_contact').removeClass('active');
	});


	$('.header__top-faq').click(function(event) {
		$('.sticky-bot-menu_info').toggleClass('active');
	});

})(jQuery);

(function PromoPopup($) {
	$('.lk-balls__bonus-btn').click(function(event) {
		event.preventDefault();
		$(this).parent().siblings('.popup-promo-code').toggle();
	});
	$('.popup-promo-code__close').click(function(event) {
		$(this).parent().hide();
	});
})(jQuery);

(function accordionFunc($) {
	$('.recipe__expand_inner').click(function(event) {
		event.preventDefault();
		$(this).parents('.panel-collapse').siblings('.panel-heading').find('.recipe__expand').click();
	});
	$('.recipe__expand').click(function(event) {
		$('.recipe__expand').each(function(index, el) {
			if ($(el).hasClass('collapsed')) {
				
				$(el).text('Раскрыть рецепт');
			}
			else {
				$(el).text('Скрыть рецепт');
			}
		});
		if ($(this).hasClass('collapsed')) {
			$(this).text('Скрыть рецепт');
		}
		else {
			$(this).text('Раскрыть рецепт');
		}
	});
	$('.modal-lk .lk-tabs__tabs  a').click(function() {
		$('.panel-collapse.in').collapse('hide');
		$('.recipe__expand').each(function(index, el) {
			$(el).text('Раскрыть рецепт');
		});
	});
})(jQuery);

(function accordionFunc2($) {
	$('.accordion').click(function(event) {
		event.preventDefault();
		$(this).siblings().removeClass('expanded');
		$(this).toggleClass('expanded');

	});
})(jQuery);

(function lkFormEmailer($) {
	$('.main-lk__havepassword').click(function(event) {
		event.preventDefault();
		var emailInp = $(this).siblings('.main-lk__email');
		var pwInp = $(this).siblings('.main-lk__pw');
		var tip = $(this).siblings('.main-lk__tip');
		var getEmailAgain = $(this).siblings('.main-lk__getpass');
		if (validateEmail(emailInp.val().trim())) {
			emailInp.hide();
			pwInp.show();
			tip.html('Введите пароль для' + ' ' + emailInp.val());
			getEmailAgain.text('повторно получить пароль на email');
			$(this).text('Войти');
		}
		else {
			alert('Введите корректный e-mail');
		}
	});
})(jQuery);

(function cutStr($, length) {
	$('.product-block__descr-town').each(function() {
		var textToCut = $(this).text().trim();
		if (textToCut.length > length) {
			if ($(document).width() < 768) {
				var cuttedText = textToCut.substring(0, length - 20);
				$(this).text(cuttedText + ' ...');
			}
			else {
				var cuttedText = textToCut.substring(0, length);
				$(this).text(cuttedText + ' ...');
			}
		}
	});
	$('.product-block__descr-title').each(function() {
		var textToCut = $(this).text().trim();
		if (textToCut.length > length - 10) {
			var cuttedText = textToCut.substring(0, length - 40);
			$(this).text(cuttedText + ' ...');
		}
	});
})(jQuery, 70);

(function addRecipe($) {
	$('.block_blog-small-underline-add').click(function(event) {
		event.preventDefault();
		if ($(this).hasClass('added')) {
			$(this).find('span:first-of-type').contents().last().replaceWith('В мои рецепты');
			$(this).find('.icon-chef-slider').addClass('vs-icon-cross').removeClass('vs-icon-checked');
			$(this).removeClass('added');
		}
		else {
			$(this).find('span:first-of-type').contents().last().replaceWith('В моих рецептах');
			$(this).find('.icon-chef-slider').removeClass('vs-icon-cross').addClass('vs-icon-checked');
			$(this).addClass('added');
		}
	});
})(jQuery);

(function clearInput($) {
	$('.searchform__clear').click(function(event) {
		event.preventDefault();
		$(this).siblings('input').val('');
	});
})(jQuery);



(function($) {
	$(window).on("load",function(){
		if ($(document).width() > 1025) {
			$("#modal-lk .modal-body").mCustomScrollbar({
				setHeight: true,
				theme: 'dark',
			});
			$("#modal-cart .modal-body").mCustomScrollbar({
				setHeight: true,
				theme: 'dark',
			});
			$("#modal-about .modal-body").mCustomScrollbar({
				setHeight: true,
				theme: 'dark',
			});
			$("#GSCCModal .modal-content").mCustomScrollbar({
				setHeight: true,
				theme: 'dark',
			});
			$("#modal-cart .modal-content").mCustomScrollbar({
				setHeight: true,
				theme: 'dark',
			});
		}
		if ($(document).width() < 768) {
			$('.block .product-block__number').selectmenu({
				icons: { button: 'ui'}
			});
			$(function() {
				FastClick.attach(document.body);
			});
		}

		// $('.sticky-bot-menu_store').click();
		// $('.sticky-bot-menu_user').click();
	});
})(jQuery);