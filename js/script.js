(function($) {
	$(document).ready(function(){

		$('.set-slider').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight:true,
			arrows: true,
			responsive: [
			{
				breakpoint: 767,
				settings: {
					dots: true,
					infinite: true,
					speed: 300,
					slidesToShow: 1,
					adaptiveHeight:true,
					arrows: false
				}
			}
			]
		});

		$('.single-item').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight:true,
			arrows: false
		});

		$('.multiple-items').slick({
			dots: true,
			infinite: false,
			slidesToShow: 3,
			adaptiveHeight:true,
			slidesToScroll: 3,
			
			responsive: [
			{
				breakpoint: 990,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
				}
			},
			{
				breakpoint: 350,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: false,  
				}
			}
			]
		}); 

		$('.multiple2-items').slick({
			dots: true,
			infinite: false,
			slidesToShow: 6,
			adaptiveHeight:true,
			slidesToScroll: 6,
			responsive: [
			{
				breakpoint: 1800,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 1190,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 990,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 510,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					dots: false
				}
			}
			]
		}); 



		var changeHeight = true;
		$('.block__fructs-list .block__midpart').click(function() {
			var parent = $(this).parents('.product-block').parent('div');
			var parents = $(this).parents('.product-block').parent('div').siblings().not('.block__fructs-expand');
			var blockHeight = Math.ceil(parent.outerHeight() + 1);
			if (changeHeight) {
				parent.height(blockHeight);
				parents.each(function(index, el) {
					$(el).height(blockHeight)
				});
				changeHeight = false;
			}
		});
		$('.product-block__descr-more').not('.main_slide').parents('.block__midpart').click(function() {
			var clicked = $(this).closest('.block__midpart').hasClass('midpart_expand');
			$('.product-block__descr__full').removeClass('descr_expand');
			$('.block__midpart').removeClass('midpart_expand');
			$('.link__arrow-holder').removeClass('arrow_change');
			if (!clicked){
				$(this).closest('.block__midpart').toggleClass('midpart_expand').find('.product-block__descr__full').toggleClass('descr_expand').closest('.block__midpart').next('.block__botpart').find('.chef__receipt').toggleClass('chef_expand');
				$(this).closest('.block__midpart').find('.link__arrow-holder').toggleClass('arrow_change');
			}
		});





		$('.custom_tab > ul li').click(function(){
			$(this).addClass('active').siblings().removeClass('active').closest('.custom_tab').find('.tab_content').hide().closest('.custom_tab').find('.tab_content').eq($(this).index()).show();
		});

		$('.block_blog-small').hover(function(){
			$(this).find( ".block_blog-small-desc" ).stop().slideToggle();
		});

		$('.block_blog-big').hover(function(){
			$(this).find( ".block_blog-big-desc" ).stop().slideToggle();
		});


		if ($(document).width() < 768) {
			$( ".footer__copyright" ).after( $( ".footer__phone" ) );
			$( ".footer__phone" ).after( $( ".footer__icons" ) );
			$( ".footer__icons" ).after( $( ".footer__copyright" ) );
		}




		$('.variable-width').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			responsive: [

			{
				breakpoint: 990,
				settings: {
					dots: true,


				}
			}

			]
		});

		$('.variable-width2').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 2,
			centerMode: true,
			variableWidth: true,
			arrows: true

		});


		$(document).on('click', '.header__bot-list a', function(event){
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				$('html, body').animate({
					scrollTop: $( $.attr(this, 'href') ).offset().top
				}, 500);
			}
		});


		var modalUniqueClass = ".modal";
		$('.modal').on('show.bs.modal', function(e) {
			var $element = $(this);
			var $uniques = $(modalUniqueClass + ':visible').not($(this));
			if ($uniques.length) {
				$uniques.modal('hide');
				$uniques.one('hidden.bs.modal', function(e) {
					$element.modal('show');
				});
				return false;
			}
		});


		$('.modal-footer__next').click(function(){
			$('#GSCCModal').modal('hide');
		});



		$(".sticky-bot-menu a").click(function(event){
			event.preventDefault();
		});

		$('.modal-footer__next a').click(function(event) {
			event.preventDefault();
			$(this).parents('.modal').modal('hide');
		});









	});
})(jQuery);